'use strict';
const axios = require('axios');
const moment = require('moment');
const Crypto = require('crypto');

const api = "https://www.billingo.hu/api";

class Billingo {
    async generateInvoice() {
        console.log('billingo generate invoice...');

        var invoice = {
            fulfillment_date: "2018-03-14",
            due_date: "2018-03-11",
            comment: "",
            payment_method: 4,//cash on delivery
            template_lang_code: "hu",
            electronic_invoice: 0, //test
            currency: "HUF",
            client_uid: 2102226206,
            block_uid: 0,
            type: 3,
            round_to: 0,
            bank_account_uid: 0,
            items: [
                {
                    description: "Syntrival tabletta",
                    vat_id: 1,
                    qty: 1,
                    net_unit_price: 944.3,
                    unit: "kiszerelés"
                },
                {
                    description: "Kedvezmény",
                    net_unit_price: -120,
                    unit: '',
                    vat_rate: 20,
                    qty: 1
                }
            ]
        }

        var token = await this.generateToken();
        console.log('$token:', token);
        if (!token) return console.log('$token null!');
        
        var client = await this.request('/clients', {
            name: "Gigazoom LLC.",
            email: "rbrooks5@amazon.com",
            taxcode: "123456",
            force: false,
            billing_address: {
                street_name: "Moulton",
                street_type: "Terrace",
                house_nr: "2797",
                block: "A",
                entrance: "B",
                floor: "3.",
                door: "17",
                city: "Preston",
                postcode: "PR1",
                district: "XII",
                country: "United Kingdom"
            },
            phone: "",
            bank: {
                account_no: "12345678-12345678-12345678",
                iban: "",
                swift: ""
            },
            fokonyv_szam: "",
            type: "2",
            defaults: {
                payment_method: "1",
                electronic_invoice: "0",
                invoice_due_days: "3",
                invoice_currency: "HUF",
                invoice_template_lang_code: "hu"
            }
        }, {
                headers: {
                    "Authorization": token,
                    "Content-Type": "application/json",
                }
            });

        invoice.client_uid = client.data.id;
        console.log('client:', client);

        var result = await this.request('/invoices', invoice, {
            headers: {
                "Authorization": token,
                "Content-Type": "application/json",
            }
        });
        console.log("#result:", result);
    }

    async request(url, data, options) {
        return new Promise((resolve) => {
            axios.post(`${api}${url}`, data, options).then(function (res) {
                resolve(res.data);
            }).catch(function (er) {
                resolve(er.response.data);
            });
        });
    }

    async generateToken() {
        return new Promise((resolve) => {
            var token;
            var public_key = config.billingo.public_key;
            var private_key = config.billingo.private_key;
            function base64url(source) {
                source = source.replace(/=+$/, '');
                source = source.replace(/\+/g, '-');
                source = source.replace(/\//g, '_');
                return source;
            }
            var header = base64url(Buffer.from(JSON.stringify({ alg: 'HS256', typ: "JWT" })).toString('base64'));
            var claims = base64url(Buffer.from(JSON.stringify({
                sub: public_key,
                iat: Date.parse(new Date()) / 1000,
                exp: (Date.parse(new Date()) / 1000) + 600
            })).toString('base64'));
            var payload = header + '.' + claims;
            const hmac = Crypto.createHmac('sha256', private_key);
            hmac.on('readable', function () {
                const data = hmac.read();
                if (data) {
                    token = 'Bearer ' + payload + '.' + base64url(data.toString('base64'));
                }
                resolve(token);
            });
            hmac.write(payload);
            hmac.end();
        });
    }
}

global.Billingo = new Billingo();